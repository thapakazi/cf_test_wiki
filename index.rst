.. ourWiki documentation master file, created by
   sphinx-quickstart on Fri Dec 27 01:01:46 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ourWiki's documentation!
===================================

Recent Topics:

.. toctree::
   :glob:
   :maxdepth: 2

   wiki/*/*

   table_of_contents


Quick Links
==================
* :ref:`create_wiki`
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
